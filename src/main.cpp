#include "TetrisConfig.h"
#include "game.h"

int main(int argc, char* argv[]) {
  Game game;
  if(game.init()) {
    game.mainLoop();
  }
  game.cleanUp();
  return 0;
}

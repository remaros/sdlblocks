#include "texture.h"
#include <string>
#include <iostream>

Texture::Texture(SDL_Renderer* renderer, const std::string& path) :
  mpRenderer(renderer),
  mpTexture(nullptr),
  mWidth(0),
  mHeight(0) {
  if (!path.empty()) {
    loadFromFile(path, false);
  }
}

Texture::Texture(SDL_Renderer* renderer, TTF_Font* font, const std::string& text) :
  mpRenderer(renderer),
  mpTexture(nullptr),
  mWidth(0),
  mHeight(0) {
  if (!text.empty()) {
    loadFromText(font, text);
  }
}

//for loading texture from ttf text
bool Texture::loadFromText(TTF_Font* font, const std::string& text) {
  //free current texure
  cleanUp();

  SDL_Color whiteColor = {255, 255, 255, 255};
  SDL_Surface* textSurface = TTF_RenderText_Solid(font, text.c_str(), whiteColor);
  if (!textSurface) {
    std::cout << "Could not render text surface! sdl_ttf Error: " << TTF_GetError() << std::endl;
  } else {
    //create texture from text
    mpTexture = SDL_CreateTextureFromSurface(mpRenderer, textSurface);
    if (!mpTexture) {
      std::cout << "Cannot create texture from front! SDL Error: " <<
                   SDL_GetError() << std::endl;
    } else {
      mWidth = textSurface->w;
      mHeight = textSurface->h;
    }
  }
  SDL_FreeSurface(textSurface);
  return mpTexture != nullptr;
}

bool Texture::loadFromFile(const std::string& path, bool colorKey = false) {
  //destroy current texture if loaded
  cleanUp();
  SDL_Texture* texture = nullptr;
  SDL_Surface* loadedSurface = IMG_Load(path.c_str());

  if (!loadedSurface) {
    std::cout << "Could not load file" << path
              << "! SDL_Image Error:" << IMG_GetError() << std::endl;
  } else {
    //do we want colorkey?
    if (colorKey) {
      SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0x00, 0xFF, 0xFF));
    }

    //create texture from surface
    texture = SDL_CreateTextureFromSurface(mpRenderer, loadedSurface);
    if (!texture) {
      std::cout << "Could not create texture from file: " << path
                << "! SDL Error: " << SDL_GetError() << std::endl;

    } else {
      mWidth = loadedSurface->w;
      mHeight = loadedSurface->h;
    }
    //destroy not needed surface
    SDL_FreeSurface(loadedSurface);
  }

  //return true if not null pointer
  mpTexture = texture;
  return texture != nullptr;
}

Uint16 Texture::getWidth() const {
  return mWidth;
}

Uint16 Texture::getHeight() const {
  return mHeight;
}

void Texture::render(SDL_Rect* source, SDL_Rect* dest) const {
  SDL_RenderCopy(mpRenderer, mpTexture, source, dest);
}

void Texture::cleanUp() {
  //if texture exists destroy it
  if (mpTexture) {
    SDL_DestroyTexture(mpTexture);
    mpTexture = nullptr;
    mWidth = 0;
    mHeight = 0;
  }
}

Texture::~Texture() {
  cleanUp();
}

#include "tetromino.h"
#include "defines.h"
#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

Tetromino::Tetromino() :
  mX(0),
  mY(0),
  mDimention(0),
  mType(TetrominoType::NONE),
  mBlocks(),
  mRotation(Rotation::DEG_0)
{
}

Tetromino::Tetromino(const TetrominoType type) {
  mX = TETROMINO_DEFAULT_SPAWN_POS_X;
  mY = TETROMINO_DEFAULT_SPAWN_POS_Y;
  mType = type;
  mRotation = Rotation::DEG_0;
  if (mType == TetrominoType::I || mType == TetrominoType::O) {
    mBlocks = Matrix(4, Row(4, 0));
    mDimention= 4;
  }
  else {
    mBlocks = Matrix( 3, Row(3, 0));
    mDimention= 3;
  }
  switch (mType) {
    case TetrominoType::I: {
      mBlocks[2][0] = 1;
      mBlocks[2][1] = 1;
      mBlocks[2][2] = 1;
      mBlocks[2][3] = 1;
      mX = TETROMINO_I_SPAWN_POS_X;
      mY = TETROMINO_I_SPAWN_POS_Y;
      break;
    }
    case TetrominoType::J: {
      mBlocks[1][0] = 2;
      mBlocks[1][1] = 2;
      mBlocks[1][2] = 2;
      mBlocks[2][2] = 2;
      break;
    }
    case TetrominoType::L: {
      mBlocks[1][0] = 3;
      mBlocks[1][1] = 3;
      mBlocks[1][2] = 3;
      mBlocks[2][0] = 3;
      break;
    }
    case TetrominoType::O: {
      mBlocks[1][1] = 4;
      mBlocks[1][2] = 4;
      mBlocks[2][1] = 4;
      mBlocks[2][2] = 4;
      break;
    }
    case TetrominoType::S: {
      mBlocks[1][1] = 5;
      mBlocks[1][2] = 5;
      mBlocks[2][0] = 5;
      mBlocks[2][1] = 5;
      break;
    }
    case TetrominoType::T: {
      mBlocks[1][0] = 6;
      mBlocks[1][1] = 6;
      mBlocks[1][2] = 6;
      mBlocks[2][1] = 6;
      break;
    }
    case TetrominoType::Z: {
      mBlocks[1][0] = 7;
      mBlocks[1][1] = 7;
      mBlocks[2][1] = 7;
      mBlocks[2][2] = 7;
      break;
    }
    default: { break; }
  }
}

Tetromino::Tetromino(const Tetromino& t) :
  mX(t.mX),
  mY(t.mY),
  mDimention(t.mDimention),
  mType(t.mType),
  mBlocks(t.mBlocks),
  mRotation(t.mRotation)
{
}

Tetromino::~Tetromino() {}

void Tetromino::rotate(const Direction dir) {
  vector<int> tmpVector;
  Matrix::iterator row;
  vector<int>::iterator col;
  Matrix tmpBlocks = mBlocks;
  int tmp;
  //transpose the matrix = needed for rotation
  for(auto i = 0; i < tmpBlocks.size(); ++i) {
    for(auto j = 0; j < tmpBlocks[i].size(); ++j) {
      mBlocks[j][i] = tmpBlocks[i][j];
    }
  }
  if (dir == Direction::LEFT) {
    //when rotation hits 4(360 deg) ( reset to 0 deg)
    //mRotation = (Rotation)((mRotation + DEG_90) % 4);
    mRotation = mRotation;
    //after transposition we need to flip rows to rotate left (so 0 row becomes 3, 1 becomes 2 )
    for (auto i = 0; i < std::floor(mBlocks.size() / 2); ++i) {
      tmpVector = mBlocks[i];
      mBlocks[i] = mBlocks[mBlocks.size() - i - 1];
      mBlocks[mBlocks.size()- i - 1] = tmpVector;
    }
    //rotate right
  } else {
    //if current rotation is 0deg then next rotation will be 270
    if (mRotation == Rotation::DEG_0) {
      mRotation = Rotation::DEG_270;
    } else {
      //mRotation = (Rotation)(mRotation - DEG_90);
      mRotation = mRotation;
    }
    //after transposition we need to flip columns to rotate right (so 0 row becomes 3, 1 becomes 2 )
    for(auto i = 0; i < mBlocks.size(); ++i) {
      for(auto j = 0; j < std::floor(mBlocks[i].size()) / 2; ++j) {
        tmp = mBlocks[i][j];
        mBlocks[i][j] = mBlocks[i][mBlocks[i].size() - j - 1];
        mBlocks[i][mBlocks[i].size() - j - 1] = tmp;
      }
    }
  }
}

void Tetromino::move(const Direction dir) {
  switch(dir) {
    case Direction::LEFT: {
      --mX;
      break;
    }
    case Direction::RIGHT: {
      ++mX;
      break;
    }
    case Direction::DOWN: {
      ++mY;
      break;
    }
  }
}

int Tetromino::getDimention() const {
  return mDimention;
}

int Tetromino::getX() const {
  return mX;
}

void Tetromino::setX(int X) {
  mX = X;
}

int Tetromino::getY() const {
  return mY;
}

void Tetromino::setY(int Y) {
  mY = Y;
}

Rotation Tetromino::getRotation() const {
  return mRotation;
}

const Matrix& Tetromino::getBlocks() const {
  return mBlocks;
}

TetrominoType Tetromino::getType() const {
  return mType;
}

void Tetromino::setType(TetrominoType type) {
  mType = type;
}

#ifndef WELL_H
#define WELL_H

#include "tetromino.h"
#include "bagGenerator.h"
#include "texture.h"
#include <vector>

class Well {
private:
  //methods
  bool checkForCollisions(const Tetromino& t) const;    // 1-collision, 0-no collision
  bool moveTetromino(Tetromino& t, Direction dir);

  void renderWell() const;
  void renderTetromino(Tetromino t) const;
  void renderGhostTetromino() const;
  void renderPile() const;
  void renderTetrominoBuffer() const;
  void renderBlock(int fromX, int fromY, int toX, int toY, TetrominoType type) const;
  void recalculateGhostTetrominoPosition();
  //members
  SDL_Renderer* mpRenderer;
  Tetromino mActiveTetromino;
  Tetromino mGhostTetromino;
  BagGenerator mBagGenerator;
  //pile that tetrominos fall onto - used tetrominos pile
  Matrix mTetrominoPile;
  //stack of incoming Tetromino
  std::vector<Tetromino> mTetrominosBuffer;
  //paths
  std::string mWellBackgroundPath;
  std::string mTetrominosBackgroundPath;
  //textures
  Texture* mpWellBackgroundTex;
  Texture* mpTetrominosBackgroundTex;
  bool mIsActiveTetromino;

public:
  Well(SDL_Surface* screen);
  Well(SDL_Renderer* renderer);
  ~Well();
  void addTetrominoToPile();
  void hardDropFallingTetromino();
  void render() const;
  bool rotateActiveTetromino(Direction dir);
  bool moveActiveTetromino(Direction dir);
  bool isTetrominoActive() const;
  int removeFullRows();
  TetrominoType spawnNextTetromino();
  Tetromino pickNextTetromino();

};
#endif // WELL_H

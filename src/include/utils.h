#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <sstream>
#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

class Utils {
   public:
        template <typename Type>
        static std::string toString(Type a)
        {
            std::ostringstream buff;
            buff << a;
            return buff.str();
        }
};
#endif // UTILS_H


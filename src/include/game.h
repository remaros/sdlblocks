#ifndef GAME_H
#define GAME_H

#include "defines.h"
#include "utils.h"
#include "well.h"
#include "texture.h"
#include "TetrisConfig.h"

#include <string>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>

class Game {
  private:
    //window
    SDL_Window* mpWindow;
    SDL_Renderer* mpRenderer;
      //textures
    Texture* mGameBackgroundTex;
    Texture* mStatsBackgroundTex;
    Texture* mLevelTex;
    Texture* mClearedRowsTex;
    Texture* mScoreTex;
    Well* mWell;
    //font
    TTF_Font* mpFont;
    int mFontSize;
    //paths to assets
    std::string mFontPath;
    std::string mStatsBackgroundPath;
    std::string mGameBackgroundPath;
    //game settings
    bool mIsFpsLimited;
    int mHeight;
    int mWidth;
    int mBpp;
    int mFps;
    //game state
    bool mRunning;
    bool mPaused;
    bool mGameOver;
    //game stats
    unsigned int mInitialGravity;
    unsigned int mCurrentGravity;
    unsigned int mLevel;
    bool mLevelUpdated;
    unsigned long mScore;
    bool mScoreUpdated;
    int mNewLevelCountdow;
    unsigned int mClearedRows;
    bool mClearedRowsUpdated;
    unsigned int mTetrominosCount[7] = {0, 0, 0, 0, 0, 0, 0};
    //other
    Uint32 mTickStart;
    SDL_Event mEvent;
    //methods
    bool loadAssets();
    void renderStaisticsBar(int X, int Y, int W, int H, int imgX);
  public:
    Game();
    ~Game();
    bool init();
    void handleEvents();
    void mainLoop();
    void render();
    void start(unsigned level);
    void pause();
    void update();
    void cleanUp();
    void tick();
    void levelUp();
    void renderFonts();
    void renderStatistics();
};
#endif // GAME_H



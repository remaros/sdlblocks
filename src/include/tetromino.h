#ifndef TETROMINO_H
#define TETROMINO_H

#include "defines.h"
#include "tetromino.h"
#include "defines.h"
#include <vector>

typedef std::vector<int> Row;
typedef std::vector<Row> Matrix;

class Tetromino {
  private:
    int mX;
    int mY;
    int mDimention;
    TetrominoType mType;
    Matrix mBlocks;
    Rotation mRotation;
  public:
    Tetromino(const TetrominoType type);
    Tetromino(const Tetromino& t);
    Tetromino();
    ~Tetromino();
    const Matrix& getBlocks() const;
    int getDimention() const;
    Rotation getRotation() const;
    TetrominoType getType() const;
    void setType(TetrominoType type);
    int getX() const;
    int getY() const;
    void setX(int X);
    void setY(int Y);
    void move(const Direction dir);
    void rotate(const Direction dir);
};
#endif // TETROMINO_H

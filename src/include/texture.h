#ifndef TEXTURE_H
#define TEXTURE_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <string>

class Texture {
public:
    Texture(SDL_Renderer* renderer, const std::string& path);
    Texture(SDL_Renderer* renderer, TTF_Font* font, const std::string& path);
    ~Texture();
    // colorkey rgb=0,255,255
    bool loadFromFile(const std::string& path, bool colorKey);
    // no colorkey
    bool loadFromText(TTF_Font* font, const std::string& text);

    void render(SDL_Rect* source, SDL_Rect* dest) const;
    Uint16 getHeight() const;
    Uint16 getWidth() const;
private:
    void cleanUp();
    SDL_Renderer* mpRenderer;
    SDL_Texture* mpTexture;
    Uint16 mWidth;
    Uint16 mHeight;
};

#endif // TEXTURE_H

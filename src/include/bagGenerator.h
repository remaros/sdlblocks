#ifndef BAGGENERATOR_H
#define BAGGENERATOR_H
#include "defines.h"
#include <vector>

class BagGenerator
{
public:
  BagGenerator();
  ~BagGenerator() {};
  TetrominoType get();
private:
  short mRemovedTetrominos;           //how many tetrominos was picked from bag
  std::vector<TetrominoType> mBag;
  std::vector<TetrominoType> shuffle();
};
#endif // BAGGENERATOR_H

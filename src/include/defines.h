#ifndef DEFINES_H
#define DEFINES_H

#define GAME_DEFAULT_RESOLUTION_X 800
#define GAME_DEFAULT_RESOLUTION_Y 600
#define GAME_DEFAULT_BPP 32
#define GAME_LIMIT_FPS true
#define GAME_DEFAULT_FPS 60

#define WELL_COLUMNS 10
#define WELL_ROWS 22
#define WELL_HIDDEN_ROWS 2
#define WELL_WIDTH 171
#define WELL_HEIGHT 341
#define WELL_GRID_SPACE_SIZE 1                              //size of space between cells of well

#define WELL_START_DRAW_X 314
#define WELL_START_DRAW_Y 129
#define WELL_TETROMINO_BUFFER_START_DRAW_X 530
#define WELL_TETROMINO_BUFFER_START_DRAW_Y 130
#define WELL_TETROMINO_BUFFER_SHIFT 9                       //defines shift of the4x4TetrominoType::s inTetrominoType:: buffer dispaly - needed for them to appea to be centered
//tetromino spawn points
#define TETROMINO_DEFAULT_SPAWN_POS_X 3
#define TETROMINO_DEFAULT_SPAWN_POS_Y 1
#define TETROMINO_I_SPAWN_POS_X 3
#define TETROMINO_I_SPAWN_POS_Y 0
#define TETROMINO_O_SPAWN_POS_X 3
#define TETROMINO_O_SPAWN_POS_Y 1

#define TETROMINO_NUMBER_OF_TYPES 7                         //how manyTetrominoType:: types there are

#define TETROMINO_NEXT_BUFFER_SIZE 3                        //size of buffer holding nextTetrominoType::s
#define TETROMINO_BLOCK_SPACE_SIZE WELL_GRID_SPACE_SIZE     //size of space between blocks ( tiles )

#define BITMAP_TETROMINO_BLOCK_TILE_SIZE 16                 //size of 1 block ofTetrominoType:: ( tile from image )
#define BITMAP_TETROMINO_BLOCK_TILE_START_X 0
#define BITMAP_TETROMINO_BLOCK_TILE_START_Y 0
#define BITMAP_PILE_BLOCK_TILE_START_X 128
#define BITMAP_PILE_BLOCK_TILE_START_Y 0

//score font starting position
#define FONT_SCORE_START_X 531
#define FONT_SCORE_START_Y 58
//lines font startign position
#define FONT_LINES_START_X 531
#define FONT_LINES_START_Y 96
//level font starting position
#define FONT_LEVEL_START_X 650
#define FONT_LEVEL_START_Y 406

//statistics bars starting positions
//each bar is 12px wide and 212px in maximum height
#define STATS_I_START_X 49
#define STATS_I_START_Y 388
#define STATS_J_START_X 71
#define STATS_J_START_Y 388
#define STATS_L_START_X 100
#define STATS_L_START_Y 388
#define STATS_O_START_X 128
#define STATS_O_START_Y 388
#define STATS_S_START_X 156
#define STATS_S_START_Y 388
#define STATS_T_START_X 191
#define STATS_T_START_Y 388
#define STATS_Z_START_X 225
#define STATS_Z_START_Y 388

#define STATS_W 12
#define STATS_H 212

enum class TetrominoType {
    NONE=0,
    I=1,
    J=2,
    L=3,
    O=4,
    S=5,
    T=6,
    Z=7,
    GHOST=8
};

enum class Rotation {
    DEG_0,
    DEG_90,
    DEG_180,
    DEG_270
};

enum class Direction{
    LEFT,
    RIGHT,
    DOWN
};

#endif // DEFINES_H

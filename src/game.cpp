#include "game.h"

using namespace std;

Game::Game() :
  mpWindow(nullptr),
  mpRenderer(nullptr),
  mGameBackgroundTex(nullptr),
  mStatsBackgroundTex(nullptr),
  mLevelTex(nullptr),
  mClearedRowsTex(nullptr),
  mScoreTex(nullptr), mWell(nullptr),
  mpFont(nullptr),
  mFontSize(22),
  mFontPath("assets/font/PressStart2P.ttf"),
  mStatsBackgroundPath("assets/images/bars.png"),
  mGameBackgroundPath("assets/images/background.png"),
  mHeight(GAME_DEFAULT_RESOLUTION_Y),
  mWidth(GAME_DEFAULT_RESOLUTION_X),
  mBpp(GAME_DEFAULT_BPP),
  mIsFpsLimited(GAME_LIMIT_FPS),
  mFps(GAME_DEFAULT_FPS),
  mGameOver(false),
  mPaused(false),
  mRunning(false),
  mClearedRows(0),
  mClearedRowsUpdated(true),
  mScore(0),
  mScoreUpdated(true),
  mLevel(1),
  mLevelUpdated(true),
  mNewLevelCountdow(5),
  mInitialGravity(1000),
  mCurrentGravity(mInitialGravity)
{
}

void Game::cleanUp() {
  if (mWell) {
    delete mWell;
    mWell = nullptr;
  }

  if (mGameBackgroundTex) {
    delete mGameBackgroundTex;
    mGameBackgroundTex = nullptr;
  }

  if (mStatsBackgroundTex) {
    delete mStatsBackgroundTex;
    mStatsBackgroundTex = nullptr;
  }

  if (mLevelTex) {
    delete mLevelTex;
    mLevelTex = nullptr;
  }

  if (mClearedRowsTex) {
    delete mClearedRowsTex;
    mClearedRowsTex = nullptr;
  }

  if (mScoreTex) {
    delete mScoreTex;
    mScoreTex = nullptr;
  }

  if (mpFont) {
    TTF_CloseFont(mpFont);
    mpFont = nullptr;
  }
  if (mpRenderer) {
    SDL_DestroyRenderer(mpRenderer);
    mpRenderer = nullptr;
  }

  if (mpWindow) {
SDL_DestroyWindow(mpWindow);
    mpWindow = nullptr;
  }
  IMG_Quit();
  TTF_Quit();
  SDL_Quit();
}

Game::~Game() {}

bool Game::init() {
  bool success = true;
  string titleBarString("sdlTetris v");
  titleBarString.append(Utils::toString(TETRIS_VERSION));

  //initialize sdl
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    cout << "Could not initialize SDL! Error: " << SDL_GetError() << endl;
    success = false;
  } else {
    //create window
    mpWindow = SDL_CreateWindow(
        titleBarString.c_str(),
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        mWidth,
        mHeight,
        SDL_WINDOW_SHOWN);
    if (!mpWindow) {
      cout << "Could not create window! Error: " << SDL_GetError() << endl;
      success = false;
    } else {
      //create renderer
      mpRenderer = SDL_CreateRenderer(mpWindow, -1, SDL_RENDERER_ACCELERATED);
      if (!mpRenderer) {
        cout << "Could not create renderer! Error: " << SDL_GetError() << endl;
        success = false;
      } else {
        //set renderer color to white
        SDL_SetRenderDrawColor(mpRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

        //initialize sdl_image wiht png support
        if (!(IMG_Init(IMG_INIT_PNG) && IMG_INIT_PNG)) {
          cout << "Could not initialize SDL_Image! Error: " << IMG_GetError() << endl;
          success = false;
        }
        //initialize sdl_ttf subsystem
        if (TTF_Init() == -1) {
          cout << "Could not initialize SDL_ttf! Error" << TTF_GetError() << endl;
          success = false;
        }
      }
    }
  }
  //load game assets
  if (!loadAssets()) {
    success = false;
  }
  //initialize well here since we cannot do it in constructor cuz of renderer beeing null
  mWell = new Well(mpRenderer);
  //set game state to running
  mRunning = true;
  return success;
}

bool Game::loadAssets() {
  //font
  mpFont = TTF_OpenFont(mFontPath.c_str(), mFontSize);
  //textures
  mGameBackgroundTex = new Texture(mpRenderer, mGameBackgroundPath);
  mStatsBackgroundTex = new Texture(mpRenderer, mStatsBackgroundPath);
  //initialize empty texture needed for text
  mScoreTex = new Texture(mpRenderer, mpFont, "");
  mClearedRowsTex = new Texture(mpRenderer, mpFont, "");
  mLevelTex = new Texture(mpRenderer, mpFont, "");

  return true;
}

void Game::mainLoop() {
  Uint32 startTime;
  Uint32 frameTime;
  while (mRunning) {
    if (mIsFpsLimited) {
      startTime = SDL_GetTicks();
    }
    handleEvents();
    update();
    render();
    if (mIsFpsLimited) {
      frameTime = SDL_GetTicks() - startTime;
      if (frameTime < (Uint32)(1000 / mFps)) {
        SDL_Delay((Uint32)(1000 / mFps) - (SDL_GetTicks() - startTime));
      }
    }
  }
}

void Game::tick() {

  if (!mWell->moveActiveTetromino(Direction::DOWN)) {
    mWell->addTetrominoToPile();
  }
  mTickStart = SDL_GetTicks();
}

void Game::update() {
  //game over?
  if (mGameOver) {
    cout << "Game Over!!" << endl;
    mRunning = false;
    //exit(EXIT_FAILURE);
    //handle game over
  } else if (mPaused) {
    cout << "PAUSED!!" << endl;
  } //game runs normally
  else {
    if (mWell->isTetrominoActive()) {
      if ((SDL_GetTicks() - mTickStart) > mInitialGravity - ((mLevel - 1) * 150))
        tick();
    } else {
      int factor = 0;
      int cleared = mWell->removeFullRows();
      switch (cleared) {
        case 1:
          factor = 1;
          break;
        case 2:
          factor = 3;
          break;
        case 3:
          factor = 5;
          break;
        case 4:
          factor = 8;
          break;
      }
      if (mScoreUpdated != (mScore + (factor * 100 * mLevel))) {
        mScoreUpdated = true;
      }
      mScore += factor * 100 * mLevel;

      mNewLevelCountdow -= factor;
      if (mNewLevelCountdow <= 0) {
        levelUp();
      }

      if (mClearedRows != (mClearedRows + cleared)) {
        mClearedRowsUpdated = true;
      }
      mClearedRows += cleared;

      TetrominoType spawned = mWell->spawnNextTetromino();
      if (spawned == TetrominoType::NONE) {
        mGameOver = true;
      } else {
        mTetrominosCount[(int)(spawned) - 1]++;
        mTickStart = SDL_GetTicks();
      }
    }
  }
}

void Game::handleEvents() {
  while (SDL_PollEvent(&mEvent)) {
    switch(mEvent.type) {
      case SDL_QUIT: {
        mRunning = false;
        break;
      }
      case SDL_KEYDOWN: {
        if (mEvent.key.keysym.sym == SDLK_LEFT) {
          mWell->moveActiveTetromino(Direction::LEFT);
        }
        if (mEvent.key.keysym.sym == SDLK_RIGHT) {
          mWell->moveActiveTetromino(Direction::RIGHT);
        }
        if (mEvent.key.keysym.sym == SDLK_DOWN) {
          mWell->moveActiveTetromino(Direction::DOWN);
        }
        if (mEvent.key.keysym.sym == SDLK_UP) {
          mWell->rotateActiveTetromino(Direction::LEFT);
        }
        if (mEvent.key.keysym.sym == SDLK_ESCAPE) {
          pause();
        }
        if (mEvent.key.keysym.sym == SDLK_SPACE) {
          mWell->hardDropFallingTetromino();
        }
        cout << "Pressed "<< mEvent.key.keysym.sym << endl;
        break;
      }
    }
  }
}

void Game::levelUp() {
  mLevel++;
  mLevelUpdated = true;
  mNewLevelCountdow = 5 * mLevel;
}

void Game::pause() {}

void Game::start(unsigned level) {
  mLevel = level;
}

void Game::renderStatistics() {
  // I
  renderStaisticsBar(STATS_I_START_X, STATS_I_START_Y, STATS_W, mTetrominosCount[0] % STATS_H, STATS_W * 0);
  // J
  renderStaisticsBar(STATS_J_START_X, STATS_J_START_Y, STATS_W, mTetrominosCount[1] % STATS_H, STATS_W * 1);
  // L
  renderStaisticsBar(STATS_L_START_X, STATS_L_START_Y, STATS_W, mTetrominosCount[2] % STATS_H, STATS_W * 2);
  // O
  renderStaisticsBar(STATS_O_START_X, STATS_O_START_Y, STATS_W, mTetrominosCount[3] % STATS_H, STATS_W * 3);
  // S
  renderStaisticsBar(STATS_S_START_X, STATS_S_START_Y, STATS_W, mTetrominosCount[4] % STATS_H, STATS_W * 4);
  // T
  renderStaisticsBar(STATS_T_START_X, STATS_T_START_Y, STATS_W, mTetrominosCount[5] % STATS_H, STATS_W * 5);
  // Z
  renderStaisticsBar(STATS_Z_START_X, STATS_Z_START_Y, STATS_W, mTetrominosCount[6] % STATS_H, STATS_W * 6);
}

void Game::renderStaisticsBar(int X, int Y, int W, int H, int imgX) {
  SDL_Rect from = {
    static_cast<Sint16>(imgX), 0,
    static_cast<Uint16>(W), static_cast<Uint16>(H)
  };
  SDL_Rect to = {
    static_cast<Sint16>(X), static_cast<Sint16>(Y - H),
    static_cast<Uint16>(W), static_cast<Uint16>(H)
  };
  mStatsBackgroundTex->render(&from, &to);
}

void Game::renderFonts() {
  int w, h;

  //score
  if (!TTF_SizeText(mpFont, Utils::toString(mScore).c_str(), &w, &h)) {
    //if text didnt change dont create new texture, use old one
    if (mScoreUpdated){
      mScoreTex->loadFromText(mpFont, Utils::toString(mScore).c_str());
      mScoreUpdated = false;
    }
    SDL_Rect dest = {
      static_cast<Sint16>(FONT_SCORE_START_X - w), static_cast<Sint16>(FONT_SCORE_START_Y - h),
      mScoreTex->getWidth(), mScoreTex->getHeight()
    };
    mScoreTex->render(nullptr, &dest);
  } else {
    cout << "Could not get text size!" << endl;
  }
  //cleared lines
  if (!TTF_SizeText(mpFont, Utils::toString(mClearedRows).c_str(), &w, &h)) {
    //if text didnt change dont create new texture, use old one
    if (mClearedRowsUpdated) {
      mClearedRowsTex->loadFromText(mpFont, Utils::toString(mClearedRows).c_str());
      mClearedRowsUpdated = false;
    }
    SDL_Rect dest = {
      static_cast<Sint16>(FONT_LINES_START_X - w), static_cast<Sint16>(FONT_LINES_START_Y - h),
      mClearedRowsTex->getWidth(), mClearedRowsTex->getHeight()
    };
    mClearedRowsTex->render(nullptr, &dest);
  } else {
    cout << "Could not get text size!" << endl;
  }
  //level
  if (!TTF_SizeText(mpFont, Utils::toString(mLevel).c_str(), &w, &h)) {
    //if text didnt change dont create new texture, use old one
    if (mLevelUpdated) {
      mLevelTex->loadFromText(mpFont, Utils::toString(mLevel).c_str());
      mLevelUpdated = false;
    }
    SDL_Rect dest = {
      static_cast<Sint16>(FONT_LEVEL_START_X - w), static_cast<Sint16>(FONT_LEVEL_START_Y - h),
      mLevelTex->getWidth(), mLevelTex->getHeight()
    };
    mLevelTex->render(nullptr, &dest);
  } else {
    cout << "Could not get text size!" << endl;
  }
}

void Game::render() {
  //render background texture
  mGameBackgroundTex->render(nullptr, nullptr);

  mWell->render();
  renderFonts();
  renderStatistics();
  SDL_RenderPresent(mpRenderer);
}


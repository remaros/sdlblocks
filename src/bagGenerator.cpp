#include "bagGenerator.h"
#include <algorithm>

using namespace std;

const vector<TetrominoType> defaultBag = {
  TetrominoType::I,
  TetrominoType::J,
  TetrominoType::L,
  TetrominoType::O,
  TetrominoType::S,
  TetrominoType::T,
  TetrominoType::Z
};

BagGenerator::BagGenerator() :
  mRemovedTetrominos(0),
  mBag(defaultBag)
{
}

vector<TetrominoType> BagGenerator::shuffle() {
  random_shuffle(mBag.begin(), mBag.end());
  return mBag;
}

TetrominoType BagGenerator::get() {
  if (!(mRemovedTetrominos %= 7)) {
    shuffle();
  }
  return mBag.at(mRemovedTetrominos++);
}

#include "well.h"
#include <algorithm>
#include <iostream>

using namespace std;

Well::Well(SDL_Renderer* renderer) :
  mpRenderer(renderer),
  mTetrominoPile(Matrix(22, Row(10, 0))),
  mGhostTetromino(mActiveTetromino),
  mIsActiveTetromino(false),
  mWellBackgroundPath(string("assets/images/board.png")),
  mpWellBackgroundTex(new Texture( mpRenderer, mWellBackgroundPath)),
  mTetrominosBackgroundPath(std::string("assets/images/blocks.png")),
  mpTetrominosBackgroundTex(new Texture(mpRenderer, mTetrominosBackgroundPath)) {
  for (unsigned i = 0; i < TETROMINO_NEXT_BUFFER_SIZE; ++i) {
    mTetrominosBuffer.push_back(pickNextTetromino());
  }
  mActiveTetromino = pickNextTetromino();
}

Well::~Well() {
  if (mpWellBackgroundTex) {
    delete mpWellBackgroundTex;
    mpWellBackgroundTex = nullptr;
  }
  if (mpTetrominosBackgroundTex) {
    delete mpTetrominosBackgroundTex;
    mpTetrominosBackgroundTex = nullptr;
  }
}

void Well::addTetrominoToPile() {
  Matrix blocks = mActiveTetromino.getBlocks();

  int dim = mActiveTetromino.getDimention();
  for (unsigned row = 0; row < dim; row++ ) {
    for (unsigned col = 0; col < dim; col++ ) {
      if (blocks[row][col] != 0) {
        mTetrominoPile[mActiveTetromino.getY() + row][mActiveTetromino.getX() + col] = blocks[row][col];
      }
    }
  }
  mIsActiveTetromino = false;
}

bool rowIsFull(const Row& row) {
  return count_if(row.begin(), row.end(), [](int val){ return val != 0;}) == WELL_COLUMNS;
}

int Well::removeFullRows() {
  int rowsRemoved = 0;
  mTetrominoPile.erase(remove_if(mTetrominoPile.begin(),
                                 mTetrominoPile.end(),
                                 rowIsFull),
                                 mTetrominoPile.end());

  rowsRemoved = WELL_ROWS - mTetrominoPile.size();
  mTetrominoPile.insert(mTetrominoPile.begin(), rowsRemoved, vector<int>(10, 0));
  return rowsRemoved;
}

bool Well::checkForCollisions(const Tetromino& t) const {
  Matrix blocks = t.getBlocks();
  int dim = t.getDimention();
  for (auto row = 0; row < dim; ++row) {
    for (auto col = 0; col < dim; ++col) {
      if (blocks[row][col] != 0) {
        if ((t.getX() + col < 0 ) ||                     // collision with left wall
            (t.getX() + col > WELL_COLUMNS - 1) ||       // collision with right wall
            (t.getY() + row > WELL_ROWS - 1) ||          // collision with top wall
            (mTetrominoPile[t.getY() + row][t.getX() + col] != 0)) {  // collision with bottom wall
          return true;
        }
      }
    }
  }
  return false;
}

Tetromino Well::pickNextTetromino() {
  Tetromino nextTetromino = Tetromino(mBagGenerator.get());
  return nextTetromino;
}

void Well::hardDropFallingTetromino() {
  while (moveActiveTetromino(Direction::DOWN));
  mIsActiveTetromino = false;
  addTetrominoToPile();
}

//if moved then return 1 else 0
bool Well::moveActiveTetromino(Direction dir) {
  if (moveTetromino(mActiveTetromino, dir)) {
    recalculateGhostTetrominoPosition();
    return true;
  }
  return false;
}

bool Well::moveTetromino(Tetromino& t, Direction dir ) {
  // check if we can move (on tmp tetromino) if yes the move it
  Tetromino tmpTetromino = t;
  tmpTetromino.move(dir);
  if (!checkForCollisions(tmpTetromino)) {
    t.move(dir);
    return true;
  }
  return false;
}

void Well::recalculateGhostTetrominoPosition() {
  mGhostTetromino.setX(mActiveTetromino.getX());
  mGhostTetromino.setY(mActiveTetromino.getY());
  while (moveTetromino(mGhostTetromino, Direction::DOWN));
}

bool Well::rotateActiveTetromino(Direction dir) {
  Tetromino tmpTetromino = mActiveTetromino;
  tmpTetromino.rotate(dir);
  if (!checkForCollisions(tmpTetromino)) {
    mGhostTetromino.rotate(dir);
    recalculateGhostTetrominoPosition();
    mActiveTetromino.rotate(dir);
    return true;
  }
  return false;
}

TetrominoType Well::spawnNextTetromino() {
  mActiveTetromino = mTetrominosBuffer.at(0);
  mGhostTetromino = mActiveTetromino;
  mGhostTetromino.setType(TetrominoType::GHOST);
  //move ghost tetromino at the top of the pile
  while( moveTetromino(mGhostTetromino, Direction::DOWN));

  // remove first buffer element
  mTetrominosBuffer.erase(mTetrominosBuffer.begin());
  // pick next one
  mTetrominosBuffer.push_back(pickNextTetromino());

  if (checkForCollisions(mActiveTetromino)) {
    return TetrominoType::NONE;
  } else {
    mIsActiveTetromino = true;
    return mActiveTetromino.getType();
  }
}

bool Well::isTetrominoActive() const {
  return mIsActiveTetromino;
}

void Well::render() const {
  renderTetrominoBuffer();
  renderWell();
  renderPile();
  renderGhostTetromino();
  renderTetromino(mActiveTetromino);
}


void Well::renderTetrominoBuffer() const {
  vector<Tetromino>::const_iterator tetrominoItr = mTetrominosBuffer.begin();
  int extra = 0;
  int nr = 0;
  while(tetrominoItr != mTetrominosBuffer.end()) {
    Matrix B = tetrominoItr->getBlocks();
    int dim = tetrominoItr->getDimention();
    TetrominoType T = tetrominoItr->getType();

    //we need to add some extra few pixels to display nextTetrominoType::s centered
    if (!((T ==TetrominoType::O) || (T == TetrominoType::I))) {
      extra = WELL_TETROMINO_BUFFER_SHIFT;
    }

    for (int row = 0; row < dim; ++row) {
      for (int col = 0; col < dim; ++col) {
        if (B[row][col] != 0)
          renderBlock(
                BITMAP_TETROMINO_BLOCK_TILE_START_X,
                BITMAP_TETROMINO_BLOCK_TILE_START_Y,
                WELL_TETROMINO_BUFFER_START_DRAW_X + extra + (col * BITMAP_TETROMINO_BLOCK_TILE_SIZE) + (col - 1) ,
                WELL_TETROMINO_BUFFER_START_DRAW_Y + (68 * nr) + (row * BITMAP_TETROMINO_BLOCK_TILE_SIZE) + (row - 1),
                T);
      }
    }
    extra = 0;
    nr++;
    tetrominoItr++;
  }
}

void Well::renderGhostTetromino() const {
  renderTetromino(mGhostTetromino);
}

void Well::renderTetromino(Tetromino t) const {
  Matrix b = t.getBlocks();
  auto dim = t.getDimention();

  //dont draw first 2 rows since they are hidden
  for (int row = 0; row < dim; ++row) {
    for (int col = 0; col < dim; ++col ) {
      if( row + t.getY() >= 2) {
        if ((b[row][col] != 0))
        {
          renderBlock(BITMAP_TETROMINO_BLOCK_TILE_START_X,
                      BITMAP_TETROMINO_BLOCK_TILE_START_Y,
                      WELL_START_DRAW_X + (BITMAP_TETROMINO_BLOCK_TILE_SIZE * (t.getX() + col)) + (t.getX() + col + 1),
                      WELL_START_DRAW_Y + (BITMAP_TETROMINO_BLOCK_TILE_SIZE * (t.getY() + row - 2)) + (t.getY() + row - 1),
                      t.getType());
        }
      }
    }
  }
}

void Well::renderBlock(int fromX, int fromY, int toX, int toY, TetrominoType type) const {
  SDL_Rect block = {
    static_cast<Sint16>(fromX + BITMAP_TETROMINO_BLOCK_TILE_SIZE * static_cast<int>(static_cast<int>(type) - 1)),
    static_cast<Sint16>(fromY),
    BITMAP_TETROMINO_BLOCK_TILE_SIZE,
    BITMAP_TETROMINO_BLOCK_TILE_SIZE
  };
  SDL_Rect dest = {
    static_cast<Sint16>(toX),
    static_cast<Sint16>(toY),
    BITMAP_TETROMINO_BLOCK_TILE_SIZE,
    BITMAP_TETROMINO_BLOCK_TILE_SIZE
  };
  mpTetrominosBackgroundTex->render(&block, &dest);
}

void Well::renderPile() const {
  for (unsigned row = WELL_ROWS - 1; row > 1; --row) {
    for (unsigned col = 0; col < WELL_COLUMNS; ++col) {
      if (mTetrominoPile[row][col] != 0) {
        renderBlock(BITMAP_PILE_BLOCK_TILE_START_X, BITMAP_PILE_BLOCK_TILE_START_Y,
                    WELL_START_DRAW_X + ( BITMAP_TETROMINO_BLOCK_TILE_SIZE * (col)) + (col + 1),
                    WELL_START_DRAW_Y + ( BITMAP_TETROMINO_BLOCK_TILE_SIZE * (row - 2)) + (row - 1),
                    static_cast<TetrominoType>(mTetrominoPile[row][col]));
      }
    }
  }
}


void Well::renderWell() const {
  SDL_Rect dest = {
    WELL_START_DRAW_X,
    WELL_START_DRAW_Y,
    WELL_WIDTH,
    WELL_HEIGHT
  };
  mpWellBackgroundTex->render(nullptr, &dest);
}

